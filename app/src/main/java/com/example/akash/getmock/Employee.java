package com.example.akash.getmock;

/**
 * Created by Akash on 25-04-2018.
 */

public class Employee
{
    private String EmployeeName;
    private String EmployeeNAddress;
    private String EmployeeNDesignation;
    private String Email;
    private String ContactNo;


    public Employee(String employeeName, String employeeNAddress, String employeeNDesignation, String email, String contactNo) {
        EmployeeName = employeeName;
        EmployeeNAddress = employeeNAddress;
        EmployeeNDesignation = employeeNDesignation;
        Email = email;
        ContactNo = contactNo;
    }

    public Employee()
    {

    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getEmployeeNAddress() {
        return EmployeeNAddress;
    }

    public void setEmployeeNAddress(String employeeNAddress) {
        EmployeeNAddress = employeeNAddress;
    }

    public String getEmployeeNDesignation() {
        return EmployeeNDesignation;
    }

    public void setEmployeeNDesignation(String employeeNDesignation) {
        EmployeeNDesignation = employeeNDesignation;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }
}
