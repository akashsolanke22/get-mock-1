package com.example.akash.getmock;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class viewemployeedetails extends AppCompatActivity {
        ListView listView;
        FirebaseDatabase database;
        DatabaseReference ref;
        ArrayList<String>list;
        ArrayAdapter<String>adapter;
        Employee employee;

    @SuppressLint("ResourceType")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewemployeedetails);
        employee=new Employee();
        listView=(ListView)findViewById(R.id.listview);
        database=FirebaseDatabase.getInstance();
        ref=database.getReference("Employee Details");
        list=new ArrayList<>();
        adapter=new ArrayAdapter<String>(this,R.id.empinfo,R.id.empinfo,list);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren())
                {
                    employee=ds.getValue(Employee.class);
                    list.add("\n EmployeeName :-"+employee.getEmployeeName().toString() + "  " + "\n EmployeeAddress :-"+employee.getEmployeeNAddress().toString() + "  " +"\n EmployeeDesignation :-"+ employee.getEmployeeNDesignation().toString() + "  "
                            +"\n Email :-"+ employee.getEmail().toString() + "  " + "\n ContactNo :-"+employee.getContactNo().toString());

                }
                    listView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
