package com.example.akash.getmock;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class addemployeedetail extends AppCompatActivity {
    EditText ename,eaddress,edesignation,email,contactno;
    Button insert;
    FirebaseDatabase database;
    DatabaseReference ref;
    helper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addemployeedetail);
        ename=(EditText)findViewById(R.id.ENAME);
        eaddress=(EditText)findViewById(R.id.eaddress);
        edesignation=(EditText)findViewById(R.id.designation);
        email=(EditText)findViewById(R.id.email);
        contactno=(EditText)findViewById(R.id.contactno);
        insert=(Button)findViewById(R.id.insert);
        database=FirebaseDatabase.getInstance();
        ref=database.getReference("Employee Details");
        helper=new helper();
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        getvalues(ename,eaddress,edesignation,email,contactno);
                        ref.child("EmpID:1").setValue(helper);
                        Toast.makeText(addemployeedetail.this,"Details Succesfully Inserted..",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                    private void getvalues(EditText ename, EditText eaddress, EditText edesignation, EditText email, EditText contactno)
                    {
                        helper.setEmployeeName(addemployeedetail.this.ename.getText().toString());
                        helper.setEmployeeNAddress(addemployeedetail.this.eaddress.getText().toString());
                        helper.setEmployeeNDesignation(addemployeedetail.this.edesignation.getText().toString());
                        helper.setContactno(addemployeedetail.this.contactno.getText().toString());

                    }
                });

            }
        });

    }
}
