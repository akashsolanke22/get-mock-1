package com.example.akash.getmock;

/**
 * Created by Akash on 24-04-2018.
 */

public class helper
{
    private String EmployeeName;
    private String EmployeeNAddress;
    private String EmployeeNDesignation;
    private String Email;
    private String Contactno;



    public helper()
    {

    }
    public helper(String employeeName, String employeeNAddress, String employeeNDesignation, String email, String contactno) {
        EmployeeName = employeeName;
        EmployeeNAddress = employeeNAddress;
        EmployeeNDesignation = employeeNDesignation;
        Email = email;
        Contactno = contactno;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getEmployeeNAddress() {
        return EmployeeNAddress;
    }

    public void setEmployeeNAddress(String employeeNAddress) {
        EmployeeNAddress = employeeNAddress;
    }

    public String getEmployeeNDesignation() {
        return EmployeeNDesignation;
    }

    public void setEmployeeNDesignation(String employeeNDesignation) {
        EmployeeNDesignation = employeeNDesignation;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getContactno() {
        return Contactno;
    }

    public void setContactno(String contactno) {
        Contactno = contactno;
    }
}
